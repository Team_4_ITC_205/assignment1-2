package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class returnBookControl {

	private returnBookUi ui;
	private enum controlState { INITIALISED, READY, INSPECTING };
	private controlState state;

	private final Library LIBRARY;
	private Loan CURRENTLOAN;


	public returnBookControl() {
		this.LIBRARY = Library.getInstance();
		state = controlState.INITIALISED;
	}


	public void setUi(returnBookUi ui) {
		if (!state.equals(controlState.INITIALISED))
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");

		this.ui = ui;
		ui.setState(returnBookUi.uiState.READY);
		state = controlState.READY;
	}


	public void bookScanned(int bookId) {
		if (!state.equals(controlState.READY))
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");

		Book currentBook = LIBRARY.getBook(bookId);

		if (currentBook == null) {
			ui.display("Invalid Book Id");
			return;
		}
		if (!currentBook.isOnLoan()) {
			ui.display("Book has not been borrowed");
			return;
		}
		CURRENTLOAN = LIBRARY.getLoanByBookId(bookId);
		double overDueFine = 0.0;
		if (CURRENTLOAN.isOverDue()) {
			overDueFine = LIBRARY.calculateOverDueFine(CURRENTLOAN);

			ui.display("Inspecting");
			ui.display(currentBook.toString());
			ui.display(CURRENTLOAN.toString());
		}
		if (CURRENTLOAN.isOverDue()) {
			ui.display(String.format("\nOverdue fine : $%.2f", overDueFine));

			ui.setState(returnBookUi.uiState.INSPECTING);
			state = controlState.INSPECTING;
		}
	}

	public void scanningComplete() {
		if (!state.equals(controlState.READY))
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");

		ui.setState(returnBookUi.uiState.COMPLETED);
	}


	public void dischargeLoan(boolean isDamaged) {
		if (!state.equals(controlState.INSPECTING))
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");

		LIBRARY.dischargeLoan(CURRENTLOAN, isDamaged);
		CURRENTLOAN = null;
		ui.setState(returnBookUi.uiState.READY);
		state = controlState.READY;
	}


}
