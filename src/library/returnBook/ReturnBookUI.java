package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

	public static enum uiState { INITIALISED, READY, INSPECTING, COMPLETED };

	private final returnBookControl returnBookControl;
	private final Scanner scanner;
	private uiState uiState;


	public ReturnBookUI(returnBookControl control) {
		this.returnBookControl = control;
		scanner = new Scanner(System.in);
		uiState = uiState.INITIALISED;
		control.setUi(this);
	}


	public void run() {
		output("Return Book Use Case UI\n");

		while (true) {

			switch (uiState) {

				case INITIALISED:
					break;

				case READY:
					String bookInputString = input("Scan Book (<enter> completes): ");
					if (bookInputString.length() == 0)
						returnBookControl.scanningComplete();

					else {
						try {
							int bookId = Integer.valueOf(bookInputString).intValue();
							returnBookControl.bookScanned(bookId);
						}
						catch (NumberFormatException e) {
							output("Invalid bookId");
						}
					}
					break;

				case INSPECTING:
					String input = input("Is book damaged? (Y/N): ");
					boolean isDamaged = false;
					if (input.toUpperCase().equals("Y"))
						isDamaged = true;

					returnBookControl.dischargeLoan(isDamaged);

				case COMPLETED:
					output("Return processing complete");
					return;

				default:
					output("Unhandled state");
					throw new RuntimeException("ReturnBookUI : unhandled state :" + uiState);
			}
		}
	}


	private String input(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}


	private void output(Object object) {
		System.out.println(object);
	}


	public void display(Object object) {
		output(object);
	}

	public void setState(uiState state) {
		this.uiState = state;
	}


}
