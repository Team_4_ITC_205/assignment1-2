package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Loan implements Serializable {
	
	public enum loanState { CURRENT, OVER_DUE, DISCHARGED }

	private final int loanId;
	private final Book book;
	private final Member member;
	private final Date date;
	private loanState state;

	
	public Loan(int loanId, Book book, Member member, Date dueDate) {
		this.loanId = loanId;
		this.book = book;
		this.member = member;
		this.date = dueDate;
		this.state = loanState.CURRENT;
	}

	
	public void checkOverDue() {
		if (state == loanState.CURRENT &&
			Calendar.getInstance().getDate().after(date))
			this.state = loanState.OVER_DUE;
		
	}

	
	public boolean isOverDue() {
		return state == loanState.OVER_DUE;
	}

	
	public Integer getId() {
		return loanId;
	}


	public Date getDueDate() {
		return date;
	}
	
	
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		return "Loan:  " + loanId + "\n" +
				"  Borrower " + member.getId() + " : " +
				member.getLastName() + ", " + member.getFirstName() + "\n" +
				"  Book " + book.getid() + " : " +
				book.gettitle() + "\n" +
				"  DueDate: " + sdf.format(date) + "\n" +
				"  State: " + state;
	}


	public Member getMember() {
		return member;
	}


	public Book getBook() {
		return book;
	}


	public void discharge() {
		state = loanState.DISCHARGED;
	}

}
